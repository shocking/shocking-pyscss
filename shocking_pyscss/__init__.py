__version__ = '0.0.1'

import logging
from pathlib import Path
import warnings

# Disable pyscss from printing warnings
with warnings.catch_warnings():
    warnings.simplefilter("ignore")
    import scss

LOGGER = logging.getLogger("shocking")


def compile_scss(document_string, output_style="expanded", search_path=""):
    compiler = scss.compiler.Compiler(output_style="expanded", search_path=search_path)

    return compiler.compile_string(document_string)


def process(file_object, extension=".scss", import_directory="style", **kwargs):
    if file_object.path.suffix != extension:
        LOGGER.debug("\'{}\' is not a SCSS stylesheet. Skipping".format(file_object.path))
        return True

    import_path = kwargs["source"].joinpath(import_directory)

    if not import_path.is_dir():
        LOGGER.error("Import directory \'{}\' does not exist".format(import_path))
        return False

    try:
        file_object.content = compile_scss(file_object.content, search_path=[import_path])
    except scss.errors.SassImportError as e:
        LOGGER.error("Failed to find import in directory \'{}\'".format(import_directory))
        LOGGER.error(e)
        return False

    LOGGER.debug("Compiled \'{}\'".format(file_object.path))

    file_object.path = file_object.path.with_suffix(".css")

    return True
