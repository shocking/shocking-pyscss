***************
shocking-pyscss
***************

`shocking`_ Plugin for the `pyscss`_ SASS Compiler

.. _`pyscss`: https://github.com/Kronuz/pyScss
.. _`shocking`: https://gitlab.com/tmose1106/shocking
