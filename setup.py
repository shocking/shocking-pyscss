from setuptools import setup


with open("README.rst", 'r') as file_object:
    long_description = file_object.read()

setup_kwargs = {
    'name': "shocking-pyscss",
    'version': "0.0.1",
    'description': "shocking Plugin for the pyscss SASS Compiler",
    'long_description': long_description,
    'long_description_content_type': "text/x-rst",
    'license': "MIT",
    'author': "Ted Moseley",
    'author_email': "tmoseley1106@gmail.com",
    'url': "https://gitlab.com/shocking/shocking-pyscss",
    'project_urls': {
        "Bug Tracker": "https://gitlab.com/shocking/shocking-pyscss/issues",
        # "Documentation": "https://docs.example.com/HelloWorld/",
        "Source Code": "https://gitlab.com/shocking/shocking-pyscss",
    },
    'packages': ["shocking_pyscss"],
    'install_requires': [
        "pyscss>=1.3"
        "shocking==0.0.1",
    ],
    'entry_points': {
        'shocking.plugin': [
            "pyscss = shocking_pyscss:process"
        ]
    },
    'python_requires': ">=3.5",
    'classifiers': (
        "Development Status :: 1 - Planning",
        "Environment :: Console",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
        "Programming Language :: Python :: 3.5",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3 :: Only",
        "Topic :: Internet :: WWW/HTTP",
        "Topic :: Software Development :: Build Tools",
    ),
}


setup(**setup_kwargs)
